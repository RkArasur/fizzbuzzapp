﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace FizzBuzzService.Services
{
    public interface IFizzBuzzRule
    {
            bool Evaluate(int number);
            string GetResult(DateTime today);

    }
}
