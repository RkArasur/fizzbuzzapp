﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;

namespace FizzBuzzService.Services
{
    public class Fizz : IFizzBuzzRule
    {
        public bool Evaluate(int number)
        {
            return (number % 3 == 0);
        }

        public string GetResult(DateTime today)
        {
            if (today.DayOfWeek != DayOfWeek.Wednesday)
            {
                return "fizz";
            }
            else
            {
                return "wizz";
            }

        }
    }
}