﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using PagedList;

namespace FizzBuzzApp.Models
{
    public class FizzBuzzViewModel
    {
        [Required]
        [Range(1, 1000, ErrorMessage = "Number should be in the range of 1 to 1000")]
        [DisplayName("Enter a number")]
        public int number { get; set; }
        public IPagedList<String> FizzBuzzNumbers { get; set; }
    }
}