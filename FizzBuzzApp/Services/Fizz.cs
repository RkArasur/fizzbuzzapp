﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;

namespace FizzBuzzApp.Services
{
    public class Fizz : IFizzBuzzRule
    {
        public bool Evaluate(int number)
        {
            return (number % 3 == 0);
        }

        public string GetResult(DayOfWeek today)
        {
            if (today != DayOfWeek.Wednesday)
            {
                return "fizz";
            }
            else
            {
                return "wizz";
            }

        }
    }
}