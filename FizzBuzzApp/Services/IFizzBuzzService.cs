﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzApp.Services
{
    public interface IFizzBuzzService
    {
        List<string> GetFizzBuzzValue(int number,DayOfWeek today);
    }
}
