﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;

namespace FizzBuzzApp.Services
{
    public class FizzBuzzService: IFizzBuzzService
    {
        private IList<IFizzBuzzRule> rules;
        public FizzBuzzService(IList<IFizzBuzzRule> rules)
        {
            this.rules = rules;
        }

        public List<string> GetFizzBuzzValue(int number,DayOfWeek today)
        {
            List<string> fizzbuzzresult = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                var result = this.rules.Where(e => e.Evaluate(i));
                if (result.Any())
                {
                    fizzbuzzresult.Add(string.Join("", result.Select(e => e.GetResult(today))));
                }
                else
                {
                    fizzbuzzresult.Add(i.ToString());
                }
            }
            return fizzbuzzresult;
        }
    }
}