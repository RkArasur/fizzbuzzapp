﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzzApp.Services;
using FizzBuzzApp.Models;
using StructureMap;
using PagedList;
using PagedList.Mvc;

namespace FizzBuzzApp.Controllers
{
    public class FizzBuzzController : Controller
    {
        private IFizzBuzzService service;

        public FizzBuzzController(IFizzBuzzService service)
        {
            this.service = service;
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View(new FizzBuzzViewModel());
        }
        [HttpPost]
        public ActionResult Index(FizzBuzzViewModel model, int? page)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("GetFizzBuzz", new { value = model.number, page = 1 });
            }
            else
            {
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult GetFizzBuzz(int value,int? page)
        {
            
            var model = new FizzBuzzViewModel();
            model.number = value;
            DateTime today = DateTime.Today;
            var fizzBuzz = this.service.GetFizzBuzzValue(model.number, today.DayOfWeek).ToList();
            model.FizzBuzzNumbers = fizzBuzz.ToPagedList(page ?? 1, 10);

            return View(model);
        }
    }
}