﻿using System;
using System.Collections.Generic;
using System.Text;
using FizzBuzzApp.Services;
using NUnit.Framework;

namespace FizzBuzzAppTests
{
    class BuzzTests
    {
        Buzz buzz;

        [SetUp]
        public void Setup()
        {
            buzz = new Buzz();
        }

        [Test]
        public void BuzzEvaluateFalse()
        {
            Assert.AreEqual(false, buzz.Evaluate(3));
        }
        [Test]
        public void BuzzEvaluateTrue()
        {
            Assert.AreEqual(true, buzz.Evaluate(5));
        }

        [Test]
        public void BuzzgetResult_Wednesday()
        {
            Assert.AreEqual("wuzz", buzz.GetResult(DayOfWeek.Wednesday));
        }

        [Test]
        public void BuzzGetResult_Not_Wednesday()
        {
            Assert.AreEqual("buzz", buzz.GetResult(DayOfWeek.Sunday));
        }
    }
}
