﻿
using System;
using System.Collections.Generic;
using System.Text;
using FizzBuzzApp.Services;
using NUnit.Framework;

namespace FizzBuzzAppTests
{
    class FizzTests
    {
        Fizz fizz;
        [SetUp]
        public void Setup()
        {
            fizz = new Fizz();
        }

        [Test]
        public void FizzEvaluateFalse()
        {
            Assert.AreEqual(false, fizz.Evaluate(5));
        }
        [Test]
        public void FizzEvaluateTrue()
        {
            Assert.AreEqual(true, fizz.Evaluate(3));
        }

        [Test]
        public void FizzGetResult_Wednesday()
        {
            Assert.AreEqual("wizz", fizz.GetResult(DayOfWeek.Wednesday));
        }

        [Test]
        public void FizzGetResult_Not_Wednesday()
        {
            Assert.AreEqual("fizz", fizz.GetResult(DayOfWeek.Sunday));
        }
    }
}
