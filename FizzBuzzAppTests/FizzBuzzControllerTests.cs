﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using FizzBuzzApp.Services;
using FizzBuzzApp.Controllers;
using System.Web.Mvc;

namespace FizzBuzzAppTests
{
    class FizzBuzzControllerTests
    {
        private class DummyFizzBuzzService : IFizzBuzzService
        {
            public List<string> GetFizzBuzzValue(int number, DayOfWeek today)
            {
                List<string> result = new List<string>();
                
                result.Add("fizzbuzz");
                result.Add("wizzwuzz");
                
                return result;
            }
        }


        [Test]
        public void CanCreateWithFizzBuzzService()
        {
            var service = new DummyFizzBuzzService();
            var controller = new FizzBuzzController(service);
            Assert.IsNotNull(controller);
        }

        [Test]
        public void Returns_Correct_View_Index()
        {
            var service = new DummyFizzBuzzService();
            var controller = new FizzBuzzController(service);

            var result = (ViewResult)controller.Index();

            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void Returns_Correct_View_GetFizzBuzz()
        {
            var service = new DummyFizzBuzzService();
            var controller = new FizzBuzzController(service);

            var result = (ViewResult)controller.GetFizzBuzz(3,2);

            Assert.AreEqual("GetFizzBuzz", result.ViewName);
        }
    }
}
