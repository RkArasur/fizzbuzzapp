using NUnit.Framework;
using System;
using System.Collections.Generic;
using FizzBuzzApp.Services;


namespace FizzBuzzAppTests
{
    public class FizzBuzzServiceTests
    {
        [Test]
        public void Solve_FizzBuzz_List_Not_Wednesday()
        {
            IList<IFizzBuzzRule> l = new List<IFizzBuzzRule>();
            l.Add(new Buzz());
            l.Add(new Fizz());
            var service = new FizzBuzzService(l);
            List<string> expected = new List<string>();
            expected.Add("1");
            expected.Add("2");
            expected.Add("fizz");
            expected.Add("4");
            expected.Add("buzz");
            expected.Add("fizz");
            Assert.AreEqual(expected, service.GetFizzBuzzValue(6, DayOfWeek.Monday));
        }

        [Test]
        public void Solve_FizzBuzz_List_Wednesday()
        {
            IList<IFizzBuzzRule> l = new List<IFizzBuzzRule>();
            l.Add(new Buzz());
            l.Add(new Fizz());
            var service = new FizzBuzzService(l);
            List<string> expected = new List<string>();
            expected.Add("1");
            expected.Add("2");
            expected.Add("wizz");
            expected.Add("4");
            expected.Add("wuzz");
            expected.Add("wizz");
            Assert.AreEqual(expected, service.GetFizzBuzzValue(6, DayOfWeek.Wednesday));
        }
    }
}